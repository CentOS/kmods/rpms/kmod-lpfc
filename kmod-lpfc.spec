%global pkg lpfc

%global kernel 5.14.0-362.18.1.el9_3
%global baserelease 2

%global debug_package %{nil}

%global __spec_install_post \
  %{?__debug_package:%{__debug_install_post}} \
  %{__arch_install_post} \
  %{__os_install_post} \
  %{__mod_compress_install_post}

%global __mod_compress_install_post %{nil}


Name:             kmod-%{pkg}
Epoch:            1
Version:          %(echo %{kernel} | sed 's/-/~/g; s/\.el.*$//g')
Release:          %{baserelease}%{?dist}
Summary:          Emulex LightPulse Fibre Channel SCSI (%{pkg}) driver

License:          GPLv2
URL:              https://www.kernel.org/

Patch0:           source-git.patch

ExclusiveArch:    x86_64 aarch64 ppc64le

BuildRequires:    elfutils-libelf-devel
BuildRequires:    gcc
BuildRequires:    kernel-rpm-macros
BuildRequires:    kmod
BuildRequires:    make
BuildRequires:    redhat-rpm-config

BuildRequires:    kernel-abi-stablelists = %{kernel}
BuildRequires:    kernel-devel-uname-r = %{kernel}.%{_arch}

Requires:         kernel-uname-r >= %{kernel}.%{_arch}

Provides:         installonlypkg(kernel-module)
Provides:         kernel-modules >= %{kernel}.%{_arch}

Requires(posttrans): %{_sbindir}/depmod
Requires(postun):    %{_sbindir}/depmod

Requires(posttrans): %{_sbindir}/weak-modules
Requires(postun):    %{_sbindir}/weak-modules

Requires(posttrans): %{_bindir}/sort
Requires(postun):    %{_bindir}/sort


%if %{epoch}
Obsoletes:        kmod-%{pkg} < %{epoch}:
%endif
Obsoletes:        kmod-%{pkg} = %{?epoch:%{epoch}:}%{version}


%description
This package provides the Emulex LightPulse Fibre Channel SCSI (%{pkg}) driver.
Compared to the in-kernel driver this driver re-enables support for some
deprecated adapters.

BladeEngine 2 (BE2) devices:
- 0x19A2:0x0704: Emulex OneConnect Tigershark FCoE

BladeEngine 3 (BE3) devices:
- 0x19A2:0x0714: Emulex OneConnect Tomcat FCoE

Fibre Channel (FC) devices:
- 0x10DF:0x1AE5: FIREFLY 
- 0x10DF:0xE100: PROTEUS VF
- 0x10DF:0xE131: BALIUS
- 0x10DF:0xE180: PROTEUS PF
- 0x10DF:0xF095: RFLY
- 0x10DF:0xF098: PFLY
- 0x10DF:0xF0A1: LP101
- 0x10DF:0xF0A5: TFLY
- 0x10DF:0xF0D1: BSMB
- 0x10DF:0xF0F5: NEPTUNE
- 0x10DF:0xF0F6: NEPTUNE SCSP
- 0x10DF:0xF0F7: NEPTUNE DCSP
- 0x10DF:0xF700: SUPERFLY
- 0x10DF:0xF800: DRAGONFLY
- 0x10DF:0xF900: CENTAUR
- 0x10DF:0xF980: PEGASUS
- 0x10DF:0xFA00: THOR
- 0x10DF:0xFB00: VIPER
- 0x10DF:0xFC00: LP10000S
- 0x10DF:0xFC10: LP11000S
- 0x10DF:0xFC20: LPE11000S
- 0x10DF:0xFC50: PROTEUS S
- 0x10DF:0xFD00: HELIOS
- 0x10DF:0xFD11: HELIOS SCSP
- 0x10DF:0xFD12: HELIOS DCSP
- 0x10DF:0xFE05: HORNET

Lancer FCoE CNA devices:
- 0x10DF:0xE208: LANCER_FC_VF
- 0x10DF:0xE260: Emulex OneConnect OCe15102-FM/OCe15104-FM/OCm15108-F-P
- 0x10DF:0xE268: LANCER_FCOE_VF


%prep
%autosetup -p1 -c -T


%build
pushd src
%{__make} -C /usr/src/kernels/%{kernel}.%{_arch} %{?_smp_mflags} M=$PWD modules
popd


%install
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/%{pkg} src/%{pkg}.ko

# Make .ko objects temporarily executable for automatic stripping
find %{buildroot}/lib/modules -type f -name \*.ko -exec chmod u+x \{\} \+

# Generate depmod.conf
%{__install} -d %{buildroot}/%{_sysconfdir}/depmod.d/
for kmod in $(find %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra -type f -name \*.ko -printf "%%P\n" | sort)
do
    echo "override $(basename $kmod .ko) * weak-updates/$(dirname $kmod)" >> %{buildroot}/%{_sysconfdir}/depmod.d/%{pkg}.conf
    echo "override $(basename $kmod .ko) * extra/$(dirname $kmod)" >> %{buildroot}/%{_sysconfdir}/depmod.d/%{pkg}.conf
done


%clean
%{__rm} -rf %{buildroot}


%triggerin -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/%{pkg}/%{pkg}.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%triggerun -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/%{pkg}/%{pkg}.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%preun
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
rpm -ql kmod-%{pkg}-%{?epoch:%{epoch}:}%{version}-%{release}.%{_arch} | grep '/lib/modules/%{kernel}.%{_arch}/.*\.ko$' >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove


%postun
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove
    if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
    then
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules --no-initramfs
    else
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules
    fi
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%posttrans
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%files
%defattr(644,root,root,755)
/lib/modules/%{kernel}.%{_arch}
%license licenses
%config(noreplace) %{_sysconfdir}/depmod.d/%{pkg}.conf


%changelog
* Fri Jul 12 2024 Peter Georg <peter.georg@physik.uni-regensburg.de> - 1:5.14.0~362.18.1-2
- Do not compress kernel modules

* Fri Feb 16 2024 Kmods SIG <sig-kmods@centosproject.org> - 1:5.14.0~362.18.1-1
- kABI tracking kmod package (kernel >= 5.14.0-362.18.1.el9_3)
